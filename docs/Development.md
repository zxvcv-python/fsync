# Development

## Checkers
Pre-commit check:
```
tox -e pre-commit
```

This check is also run during execution of `tox`

## Build
> require tox: `pip install tox`

Build all with single command run:
```
tox
```

Build wheel for specific python:
```
tox -e py310
tox -e py311
tox -e py312
```

## Publish
> require tokens for pypi and testpypi in ~/.pypirc file

### publish on pypi
```
tox -e publish
```

### publish on testpypi
```
tox -e publish-test
```
