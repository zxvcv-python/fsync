import sys


def main(argv=sys.argv[1:]):
    print("Hello fsync!")


if __name__.rpartition(".")[-1] == "__main__":
    sys.exit(main(sys.argv[1:]))
